# DIOT

DIOT is a Framework to create IOT applications on Espressif chips. It gives the user the ability to easily create a IOT application with a web interface and a MQTT client.

# Version history

## 1.0.6

- **Feature:** Added a 'period' argument to GPIO Digital Output classes. This can be used to turn on or off a GPIO automatically after some period of time
- **Feature:** Decreased the size of the "runtime" string so it looks better on mobile phones
- **Improvement:** Added this `README.md`
- **Improvement:** Convert some methods as const methods
- **Improvement:** Added a 'minimum available heap space' to the runtime information
- **Improvement:** Added the sketch size to the runtime information

## 1.0.4

- **Bugfix:** A `break` was missing from the code
- **Feature:** Added a setting type to get the return of a method
- **Feature:** Added a 'runtime information' block to the default about page

## 1.0.2

- **Improvement:** Created a base class for GPIOs to decrease code replication
- **Improvement:** Created dervices classes for Digital Input and Digital Output GPIOs
- **Improvement:** Removed the LED and Relay classes in favor of the Digital In- and Output GPIO classes
- **Feature:** Added a static reload method to the DIOT base class

## 1.0.0

- Inital release