#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Script to convert a HTML page to a C++ string
#-------------------------------------------------------------------------------
# Imports
import glob
import os
#-------------------------------------------------------------------------------
# Settings
sdir = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[0:-1])
input_directory = "{sdir}/html".format(sdir = sdir)
output_files = sdir + "/src/html/{{filename}}.h"
template = "const char tpl_{{name}}[] = \"{{cnt}}\";"
#-------------------------------------------------------------------------------
if __name__ == '__main__':
    # Loop through the files
    files = glob.glob(input_directory + '/*')
    for f in files:
        # Open the file
        with open(f) as input_file:
            # Save the contents
            cnt = ''.join([ x.strip() for x in input_file.readlines() ])

            # Get the filename
            fsplitted = f.split('/')
            filename = fsplitted[-1]

            # Add escapes to the contents
            cnt = cnt.replace('"', '\\"')
            cnt = cnt.replace('\n', '\\n');

            # Make the output
            output = template
            output = output.replace('{{name}}', filename.split('.')[0])
            output = output.replace('{{cnt}}', cnt)

            # Make the output filename
            output_fn = output_files
            output_fn = output_fn.replace('{{filename}}', filename)

            # Write the result
            with open(output_fn, 'w') as output_file:
                output_file.write(output)
#-------------------------------------------------------------------------------
