#ifndef __diot_webserver_page_h__
#define __diot_webserver_page_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include "types.h"
//------------------------------------------------------------------------------
namespace diot
{
    class DIOT_WebServerPage
    {
    public:
        // Constructors and destructors
        DIOT_WebServerPage();

        // Page properties
        uint8_t type;
        char title[16];
        char url[16];
        bool enabled;
        bool visible;
    };
}
//------------------------------------------------------------------------------
#endif