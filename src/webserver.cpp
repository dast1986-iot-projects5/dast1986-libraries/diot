// Includes
#include "webserver.h"
// Include the HTML pages
#include "html/page.html.h"
#include "html/block.html.h"
#include "html/setting_text.html.h"
#include "html/submit_button.html.h"
#include "html/page_saved.html.h"
#include "html/menuitem.html.h"
#include "html/setting_info_text.html.h"
#include "html/setting_bool.html.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_WebServer::DIOT_WebServer(char *app_name, DIOT_ConfigBlock *cfglist, uint16_t *cfglist_size)
    {
        // Set up the logger
        __logger = DIOT_Logger::get_instance();
        __logger->debug("DIOT_WebServer", "Constructor started");

        // The constructor sets the default values
        __cfglist = cfglist;
        __cfglist_size = cfglist_size;
        __setup_mode = false;

        // Set the application name
        __app_name = app_name;

        // Configure the pages
        __logger->debug("DIOT_WebServer", "Configuring pages");

        strcpy(__pages[0].title, "Information");
        strcpy(__pages[0].url, "/info");
        __pages[0].type = TYPE_INFO;
        strcpy(__pages[1].title, "Configuration");
        strcpy(__pages[1].url, "/config");
        __pages[1].type = TYPE_CONFIG;
        strcpy(__pages[2].title, "About");
        strcpy(__pages[2].url, "/about");
        __pages[2].type = TYPE_ABOUT;
        __homepage = 0;

        // Done
        __logger->debug("DIOT_WebServer", "Constructor done");
    }
    //--------------------------------------------------------------------------
    int16_t DIOT_WebServer::find_empty_configblock()
    {
        // Find the first non configured configblock
        for (uint8_t i = 0; i < *__cfglist_size; i++)
        {
            if (!__cfglist[i].configured)
            {
                // Found the number; return it
                return i;
            }
        }

        // Return a negative number
        return -1;
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::setup_mode(bool setup_mode)
    {
        // Sets the mode for the webserver. Can be two things:
        // - setup mode: only display the "config" page
        // - normal mode: display all pages
        if (setup_mode)
        {
            __logger->debug("DIOT_WebServer", "Configuring webserver for Setup Mode");
        }
        else
        {
            __logger->debug("DIOT_WebServer", "Configuring webserver for Normal Mode");
        }

        // Save the setup mode flag
        __setup_mode = setup_mode;

        // Set the correct pages for the correct mode
        if (__setup_mode)
        {
            // If we are in setup mode, we disable some pages
            __pages[0].enabled = false;
            __pages[2].enabled = false;

            // We set the configuration page as default
            __homepage = 1;
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::add_configuration()
    {
        // Method to add the configuration block

        // Get the next available position for the configblock
        int16_t configblock_location = find_empty_configblock();

        // Add the block
        if (configblock_location >= 0)
        {
            // Add the block
            char buffer[6];
            __logger->debug("DIOT_WiFi", "Adding a ConfigBlock at position: ", itoa(configblock_location, buffer, 10));

            // Configure the block
            __cfglist[configblock_location].define("Webserver configuration", "webserver", TYPE_CONFIG);
            __cfglist[configblock_location].needed_in_setup = false;
            __cfglist[configblock_location].settings[0].define_uint16("port", "Port number", (uint16_t)80);
            __cfglist[configblock_location].settings[1].define_text("username", "Username", "");
            __cfglist[configblock_location].settings[2].define_text("password", "Password", "", true);

            // Set the internal variables for the settings
            __port = __cfglist[configblock_location].settings[0].get_pointer_uint16();
            __username = __cfglist[configblock_location].settings[1].get_pointer_string();
            __password = __cfglist[configblock_location].settings[2].get_pointer_string();

            // Retrieve the settings
            __cfglist[configblock_location].retrieve_settings();
        }
        else
        {
            // Send a debug logging so the programmer knows to fix the issue
            __logger->error("DIOT_WebServer", "No free ConfigBlocks available! Increase the CFGLIST_SIZE size!");
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::set_routing()
    {
        // Methods to set all the routing for the webserver

        __logger->debug("DIOT_WebServer", "Setting up routing");

        for (uint8_t i = 0; i < PAGE_COUNT; i++)
        {
            if (__pages[i].enabled)
            {
                // Add the routing for the normal URL
                char buffer[4];
                __logger->debug("DIOT_WebServer", "Setting up page ", itoa(i, buffer, 10), " with title ", __pages[i].title);
                __web_server.on(__pages[i].url, HTTP_GET, std::bind(&DIOT_WebServer::webserver_serve_page, this, i, false));

                // Add the routing for the save URL if this is a configpage
                if (__pages[i].type == TYPE_CONFIG)
                {
                    __logger->debug("DIOT_WebServer", "This is a configpage, adding '/save' URL");

                    // Create the URL
                    char save_url[21];
                    strcpy(save_url, __pages[i].url);
                    strcat(save_url, "/save");

                    // Add it to the routing
                    __web_server.on(save_url, HTTP_POST, std::bind(&DIOT_WebServer::webserver_serve_page, this, i, true));
                }

                // Set as homepage
                if (i == __homepage)
                {
                    __logger->debug("DIOT_WebServer", "This is the homepage! Setting it as such");
                    __web_server.on("/", HTTP_GET, std::bind(&DIOT_WebServer::webserver_serve_page, this, i, false));
                }
            }
        }

        __logger->debug("DIOT_WebServer", "Routing is set");
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::begin()
    {
        // Method to start the webserver

        // If we are in 'setup' mode, we start the server on port 80. Otherwise,
        // we start the server on the configured port
        uint16_t port = 80;
        if (!__setup_mode)
        {
            port = *__port;
        }

        char buffer[6];
        __logger->debug("DIOT_WebServer", "Starting webserver on port ", itoa(port, buffer, 10));

        // Set routing
        set_routing();

        // Start the webserver
        __web_server.begin(port);
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::loop()
    {
        // Loop for the webserver
        __web_server.handleClient();
    }
    //--------------------------------------------------------------------------
    void DIOT_WebServer::webserver_serve_page(uint8_t page_id, boolean save /* = false */)
    {
        // If a password is entered in the config, and the credentials are not
        // yet provided, we have the ask the user. We don't do this if there is
        // no password configured or when we are in setup mode.
        if (__web_server.authenticate(__username, __password) || strcmp(__password, "") == 0 || strcmp(__username, "") == 0 || __setup_mode)
        {
            // The method that gets started when the user opens a page
            char log_buffer[4];
            __logger->debug("DIOT_WebServer", "Requested page ", itoa(page_id, log_buffer, 10));

            if (save)
            {
                __logger->debug("DIOT_WebServer", "This is a savepage");
            }

            // Make sure the buffer for configurationblock is empty
            strcpy(__temp_html_cfg_blocks_buffer, "");

            // Find the correct configurationblocks for this page
            for (uint16_t i = 0; i < *__cfglist_size; i++)
            {
                if (__cfglist[i].configured)
                {
                    // Check if this block is of the same type of the page
                    if (__cfglist[i].type == __pages[page_id].type)
                    {
                        // Check if we should display this page
                        if ((__setup_mode && __cfglist[i].needed_in_setup) || (!__setup_mode))
                        {
                            if (!save)
                            {
                                __logger->debug("DIOT_WebServer", "Block \"", __cfglist[i].title, "\" should be displayed on this page");

                                // Loop through the settings and add them to a string
                                char setting_buffer[1024] = "";

                                for (uint16_t s = 0; s < __cfglist[i].setting_size; s++)
                                {
                                    if (__cfglist[i].settings[s].configured && __cfglist[i].settings[s].visible)
                                    {
                                        // Create a buffer for the setting
                                        char buffer_setting[BUFFER_SIZE_SETTING] = "";

                                        // Create a buffer for the setting name
                                        char buffer_setting_name[34] = "";
                                        strcat(buffer_setting_name, __cfglist[i].id);
                                        strcat(buffer_setting_name, "_");
                                        strcat(buffer_setting_name, __cfglist[i].settings[s].id);

                                        // Do the correct action for the correct type
                                        switch (__cfglist[i].settings[s].type)
                                        {
                                        case TYPE_TEXT_PASSWORD:
                                        case TYPE_TEXT:
                                        {
                                            // Set the correct 'input type'
                                            char input_type[9] = "text";
                                            if (__cfglist[i].settings[s].type == TYPE_TEXT_PASSWORD)
                                            {
                                                strcpy(input_type, "password");
                                            }

                                            // Set the content for the buffer
                                            sprintf(buffer_setting, tpl_setting_text, __cfglist[i].settings[s].title, input_type, __cfglist[i].settings[s].string_value, buffer_setting_name);
                                            break;
                                        }
                                        case TYPE_BOOL:
                                        {
                                            // Check if we should check the checkbox
                                            char checked[8] = "checked";
                                            if (!__cfglist[i].settings[s].bool_value)
                                            {
                                                strcpy(checked, "");
                                            }
                                            sprintf(buffer_setting, tpl_setting_bool, buffer_setting_name, buffer_setting_name, checked, buffer_setting_name, __cfglist[i].settings[s].title);
                                            break;
                                        }
                                        case TYPE_UINT16:
                                        {
                                            // Set the content for the buffer
                                            char int_buffer[7];
                                            sprintf(buffer_setting, tpl_setting_text, __cfglist[i].settings[s].title, "text", itoa(__cfglist[i].settings[s].uint16_value, int_buffer, 10), buffer_setting_name);
                                            break;
                                        }
                                        case TYPE_INFO_STRING:
                                            sprintf(buffer_setting, tpl_setting_info_text, __cfglist[i].settings[s].title, __cfglist[i].settings[s].pointer_string);
                                            break;
                                        case TYPE_INFO_UINT16:
                                        {
                                            char itoa_buffer[6];
                                            sprintf(buffer_setting, tpl_setting_info_text, __cfglist[i].settings[s].title, itoa(*__cfglist[i].settings[s].pointer_uint16, itoa_buffer, 10));
                                            break;
                                        }
                                        case TYPE_INFO_FUNCTION_STRING:
                                        {
                                            char string_buffer[64];
                                            __cfglist[i].settings[s].function_string(string_buffer);
                                            sprintf(buffer_setting, tpl_setting_info_text, __cfglist[i].settings[s].title, string_buffer);
                                            break;
                                        }
                                        }

                                        // Add the setting to the buffer
                                        strcat(setting_buffer, buffer_setting);
                                    }
                                }

                                // Create a buffer for the block
                                char block_buffer[BUFFER_SIZE_BLOCK];

                                // Copy the string to the block
                                sprintf(block_buffer, tpl_block, __cfglist[i].title, setting_buffer);

                                // Add it the buffer for cfgblocks
                                strcat(__temp_html_cfg_blocks_buffer, block_buffer);
                            }
                            else
                            {
                                __logger->debug("DIOT_WebServer", "Saving settings for block \"", __cfglist[i].title, "\"");

                                // Loop through the settings
                                for (uint16_t s = 0; s < __cfglist[i].setting_size; s++)
                                {
                                    // Create a buffer for the setting name
                                    char buffer_setting_name[34] = "";
                                    strcat(buffer_setting_name, __cfglist[i].id);
                                    strcat(buffer_setting_name, "_");
                                    strcat(buffer_setting_name, __cfglist[i].settings[s].id);

                                    if (__cfglist[i].settings[s].configured && __cfglist[i].settings[s].visible)
                                    {
                                        __logger->debug("DIOT_WebServer", "Saving settings \"", __cfglist[i].settings[s].title, "\" in argument \"", buffer_setting_name, "\"");
                                    }

                                    // Save the setting in the 'setting' object of the configblock
                                    switch (__cfglist[i].settings[s].type)
                                    {
                                    case TYPE_TEXT_PASSWORD:
                                    case TYPE_TEXT:
                                        strcpy(__cfglist[i].settings[s].string_value, __web_server.arg(buffer_setting_name).c_str());
                                        break;
                                    case TYPE_BOOL:
                                        __cfglist[i].settings[s].bool_value = __web_server.hasArg(buffer_setting_name);
                                        break;
                                    case TYPE_UINT16:
                                        __cfglist[i].settings[s].uint16_value = __web_server.arg(buffer_setting_name).toInt();
                                        break;
                                    }
                                }
                            }

                            // If this is a 'save' page, we can save the settings to
                            // flash memory now
                            if (save)
                            {
                                __cfglist[i].save_settings();
                            }
                        }
                    }
                }
            }

            // Create the site title
            char site_title[64] = "Daryl IOT - ";
            strcat(site_title, __app_name);

            // Create a buffer for the form action
            char form_action[21];
            strcpy(form_action, __pages[page_id].url);
            strcat(form_action, "/save");

            // Create a buffer for the submit buttons, but only if needed
            char submit_button[64] = "";
            if (__pages[page_id].type == TYPE_CONFIG && !save)
            {
                strcpy(submit_button, tpl_submit_button);
            }

            // If we saved the config, we use the 'submit_button' buffer to display
            // a message to the user
            if (save)
            {
                strcpy(submit_button, tpl_page_saved);
            }

            // If we are not in setup mode, we have to add the menu
            char menu[BUFFER_SIZE_MENU] = "";
            if (!__setup_mode && !save)
            {
                __logger->debug("DIOT_WebServer", "Creating menu");
                for (unsigned short page_count = 0; page_count < PAGE_COUNT; page_count++)
                {
                    if (__pages[page_count].visible && __pages[page_count].enabled)
                    {
                        __logger->debug("DIOT_WebServer", "Creating menuitem for page ", __pages[page_count].title);
                        char temp_buffer[64];
                        sprintf(temp_buffer, tpl_menuitem, __pages[page_count].url, __pages[page_count].title);
                        strcat(menu, temp_buffer);
                    }
                }
            }

            // Fill the temporary buffer with the page contents
            sprintf(__temp_html_page_buffer, tpl_page, site_title, __app_name, __pages[page_id].title, form_action, menu, __temp_html_cfg_blocks_buffer, submit_button);

            // Log the size of the resulting string
            char size[5];
            __logger->debug("DIOT_WebServer", "Total page size is: ", itoa(strlen(__temp_html_page_buffer), size, 10));

            // Send the acutal page to the client
            __web_server.send_P(200, "text/html", __temp_html_page_buffer, strlen(__temp_html_page_buffer));

            // If we saved the config, we have to reload the device
            if (save)
            {
                // Turn on the onboard LED
                GPIO_Digital_Output onboard_led;
                onboard_led.set_gpio(LED_BUILTIN);
                onboard_led.on();

                // Log a message, start the timeout and restart the device
                __logger->info("DIOT_WebServer", "Configuration saved, going to restart the device");
                delay(RELOAD_TIME);
                DIOT::restart();
            }
            else
            {
                // If it isn't a saving-page, we just delay 100msec. If we don't
                // do this, the webserver gets unstable and will return RST's
                // from time to time.
                __logger->debug("DIOT_WebServer", "Starting delay");
                delay(100);
            }
        }
        else
        {
            // Authentication is needed
            __web_server.requestAuthentication(DIGEST_AUTH, "Authentication required", "Username or password is not correct");
        }
    }
}
//------------------------------------------------------------------------------