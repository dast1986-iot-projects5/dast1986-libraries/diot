// Includes
#include "logger.h"
//------------------------------------------------------------------------------
namespace diot
{
    // The default value for the __instance is 0. We override it as soon as a
    // instance gets created
    DIOT_Logger *DIOT_Logger::__instance = 0;
    //--------------------------------------------------------------------------
    DIOT_Logger *DIOT_Logger::get_instance()
    {
        // The 'getInstance' methods returns a pointer to the the first created
        // object of this class, or if that isn't done yet, it creates the first
        // object and return the pointer to this one. This way, we can make sure
        // there will only be one object of this class at all times.
        if (__instance == 0)
        {
            __instance = new DIOT_Logger();
        }

        return __instance;
    }
    //--------------------------------------------------------------------------
    DIOT_Logger::DIOT_Logger()
    {
        // Set the flag for the started serial to false
        __serial_started = false;

        // Set the Serial Severity
        set_serial_severity(DEFAULT_LOGLEVEL);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::set_serial_severity(uint8_t serial_severity)
    {
        // Method to set the Serial Severity

        // Set the serial severity
        __serial_severity = serial_severity;

        // Start the serial, if needed
        if (__serial_severity <= DEBUG)
        {
            // Start the Serial
            if (!__serial_started)
            {
                Serial.begin(9600);
                __serial_started = true;

                // Done, send a few newlines to make sure the "weird" symbols are gone
                for (uint8_t i = 0; i < 8; i++)
                {
                    Serial.println();
                }
            }
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::log(uint8_t severity, const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        // Create a buffer for the logline
        char logline[128];
        char severity_str[4];

        // Set the severity string
        switch (severity)
        {
        case EMERGENCY:
            strcpy(severity_str, "ERG");
            break;
        case ALERT:
            strcpy(severity_str, "ALR");
            break;
        case CRITICAL:
            strcpy(severity_str, "CRT");
            break;
        case ERROR:
            strcpy(severity_str, "ERR ");
            break;
        case WARNING:
            strcpy(severity_str, "WRN");
            break;
        case NOTICE:
            strcpy(severity_str, "NTC");
            break;
        case INFO:
            strcpy(severity_str, "INF");
            break;
        case DEBUG:
            strcpy(severity_str, "DBG");
            break;
        default:
            strcpy(severity_str, "UNK");
        }

        // Calculate the runtime
        double runtime = (double)millis() / 1000;

        // Set the first line
        strcpy(logline, line1);

        // Add the rest of the arguments
        if (line2)
        {
            strcat(logline, line2);
        }
        if (line3)
        {
            strcat(logline, line3);
        }
        if (line4)
        {
            strcat(logline, line4);
        }
        if (line5)
        {
            strcat(logline, line5);
        }
        if (line6)
        {
            strcat(logline, line6);
        }

        // Create the full logline
        char fulllogline[256];
        sprintf(fulllogline, "%011.3f - %s - %s - %s", runtime, module, severity_str, logline);

        // Send the created logline to the correct channels
        if (severity <= __serial_severity && __serial_severity <= DEBUG)
        {
            // Send to the Serial output
            Serial.println(fulllogline);
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::emergency(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(EMERGENCY, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::alert(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(ALERT, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::critical(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(CRITICAL, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::error(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(ERROR, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::warning(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(WARNING, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::notice(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(NOTICE, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::info(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(INFO, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
    void DIOT_Logger::debug(const char *module, const char *line1, const char *line2 /* = NULL */, const char *line3 /* = NULL */, const char *line4 /* = NULL */, const char *line5 /* = NULL */, const char *line6 /* = NULL */)
    {
        log(DEBUG, module, line1, line2, line3, line4, line5, line6);
    }
    //--------------------------------------------------------------------------
}
//------------------------------------------------------------------------------