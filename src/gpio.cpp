// Includes
#include "gpio.h"
//------------------------------------------------------------------------------
namespace diot
{
    GPIO::GPIO(uint8_t gpio /* = 0 */)
    {
        // Default constructor sets default values
        set_gpio(gpio);
    }
    //--------------------------------------------------------------------------
    void GPIO::set_gpio(uint8_t gpio)
    {
        // Method to set the GPIO
        _gpio = gpio;

        // Initialize the GPIO
        if (gpio > 0)
        {
            this->init_gpio();
        }
    }
}
//------------------------------------------------------------------------------