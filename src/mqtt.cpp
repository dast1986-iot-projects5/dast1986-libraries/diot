#include "mqtt.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_MQTT_Client::DIOT_MQTT_Client(DIOT_ConfigBlock *cfglist, uint16_t *cfglist_size)
        : __mqtt_client(__wifi_client)
    {
        // Set up the logger
        __logger = DIOT_Logger::get_instance();
        __logger->debug("DIOT_MQTT_Client", "Constructor started");

        // The constructor sets the default values
        __cfglist = cfglist;
        __cfglist_size = cfglist_size;
        status_update_timeout = DEFAULT_STATUS_UPDATE_TIMEOUT;
        __connects = 0;

        // Set connected to false
        __connected_on_boot = false;
    }
    //--------------------------------------------------------------------------
    int16_t DIOT_MQTT_Client::find_empty_configblock()
    {
        // Find the first non configured configblock
        for (uint8_t i = 0; i < *__cfglist_size; i++)
        {
            if (!__cfglist[i].configured)
            {
                // Found the number; return it
                return i;
            }
        }

        // Return a negative number
        return -1;
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::add_configuration()
    {
        // Method to add the configuration block

        // Get the next available position for the configblock
        int16_t configblock_location = find_empty_configblock();

        // Add the block
        if (configblock_location >= 0)
        {
            // Add the block
            char buffer[6];
            __logger->debug("DIOT_MQTT_Client", "Adding a ConfigBlock at position: ", itoa(configblock_location, buffer, 10));

            // Configure the block
            __cfglist[configblock_location].define("MQTT client configuration", "mqtt", TYPE_CONFIG);
            __cfglist[configblock_location].needed_in_setup = false;
            __cfglist[configblock_location].settings[0].define_bool("enabled", "Enabled", false);
            __cfglist[configblock_location].settings[1].define_text("server", "Server address", "");
            __cfglist[configblock_location].settings[2].define_uint16("port", "Port number", (uint16_t)1883);
            __cfglist[configblock_location].settings[3].define_text("clientid", "Client ID", "");
            __cfglist[configblock_location].settings[4].define_text("username", "Username", "");
            __cfglist[configblock_location].settings[5].define_text("password", "Password", "", true);
            __cfglist[configblock_location].settings[6].define_text("topic", "Topic prefix", "house/room/application/");

            // Set the internal variables for the settings
            __enabled = __cfglist[configblock_location].settings[0].get_pointer_bool();
            __server = __cfglist[configblock_location].settings[1].get_pointer_string();
            __port = __cfglist[configblock_location].settings[2].get_pointer_uint16();
            __clientid = __cfglist[configblock_location].settings[3].get_pointer_string();
            __username = __cfglist[configblock_location].settings[4].get_pointer_string();
            __password = __cfglist[configblock_location].settings[5].get_pointer_string();
            __topic = __cfglist[configblock_location].settings[6].get_pointer_string();

            // Retrieve the settings
            __cfglist[configblock_location].retrieve_settings();
        }
        else
        {
            // Send a debug logging so the programmer knows to fix the issue
            __logger->error("DIOT_MQTT_Client", "No free ConfigBlocks available! Increase the CFGLIST_SIZE size!");
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::begin(DIOT *application)
    {
        // The method that starts the MQTT client

        // Connect, if needed
        if (__enabled && strcmp(__server, "") != 0 && __port > 0 && strcmp(__clientid, "") != 0)
        {
            // Set the application pointer
            __application = application;

            // Connect
            connect();
        }
        else
        {
            __logger->notice("DIOT_MQTT_Client", "MQTT not configured for connecting; skipping");
        }
    }
    //--------------------------------------------------------------------------
    bool DIOT_MQTT_Client::connect()
    {
        // The method to actually connect

        char buffer[6];
        __logger->info("DIOT_MQTT_Client", "Connecting to MQTT broker ", __server, ":", itoa(*__port, buffer, 10));

        // Increase the counter for connections
        __connects++;

        // Create a buffer for the 'last will' topic
        char buffer_last_will_topic[128];
        cpy_topic(buffer_last_will_topic, TOPIC_STATUS_AVAILABILITY);

        // Configure the client
        __mqtt_client.setKeepAlive(MQTT_KEEPALIVE_TIMEOUT);

        // Set the server and port
        __mqtt_client.setServer(__server, *__port);

        // Connect
        if (__mqtt_client.connect(
                __clientid,
                __username,
                __password,
                buffer_last_will_topic,
                0,
                true,
                PAYLOAD_STATUS_AVAILABILITY_OFFLINE,
                false))
        {
            __logger->info("DIOT_MQTT_Client", "Connected!");

            // Connected!
            __connected_on_boot = true;

            // Update the status
            update_status();

            // Update the about section
            update_about();

            // Set a callback for subscriptions
            __mqtt_client.setCallback(std::bind(&DIOT_MQTT_Client::subscription_callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

            // Subscribe to the command topic
            cpy_topic(__topic_commands, "cmd/#");
            __mqtt_client.subscribe(__topic_commands);

            // Return true
            return true;
        }
        else
        {
            // We couldn't connect, return false so the calling method can do
            // something about it
            // Find the correct reason for not being able to connect
            char error[19];
            switch (__mqtt_client.state())
            {
            case MQTT_CONNECTION_TIMEOUT:
                strcpy(error, "Connection timeout");
                break;
            case MQTT_CONNECTION_LOST:
                strcpy(error, "Connection lost");
                break;
            case MQTT_CONNECT_FAILED:
                strcpy(error, "Connection failed");
                break;
            case MQTT_DISCONNECTED:
                strcpy(error, "Disconnected");
                break;
            case MQTT_CONNECT_BAD_PROTOCOL:
                strcpy(error, "Bad protocol");
                break;
            case MQTT_CONNECT_BAD_CLIENT_ID:
                strcpy(error, "Bad client ID");
                break;
            case MQTT_CONNECT_UNAVAILABLE:
                strcpy(error, "Unavailable");
                break;
            case MQTT_CONNECT_BAD_CREDENTIALS:
                strcpy(error, "Bad credentials");
                break;
            case MQTT_CONNECT_UNAUTHORIZED:
                strcpy(error, "Unauthorized");
                break;
            }

            // Log a error
            __logger->error("DIOT_MQTT_Client", "Couldn't connect to MQTT broker: ", error);

            return false;
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::loop()
    {
        // The loop method; checks if we are still connected if we aren't, we
        // reconnect. We also execute the "loop" method of the MQTT client
        // object
        if (__connected_on_boot)
        {
            // If we are not connected, we have to reconnect
            if (!__mqtt_client.connected())
            {
                __logger->notice("DIOT_MQTT_Client", "We lost connection to the MQTT broker, reconnecting!");

                if (connect())
                {
                    __logger->info("DIOT_MQTT_Client", "Reconnected!");
                }
            }
            // Run the 'loop' command
            __logger->debug("DIOT_MQTT_Client", "Looping");
            __mqtt_client.loop();

            // Update the device status
            unsigned long seconds_since_update = (millis() - __last_status_update) / 1000;
            if (seconds_since_update >= status_update_timeout)
            {
                update_status();
            }
        }
    }
    //--------------------------------------------------------------------------
    bool DIOT_MQTT_Client::send_message(const char *topic, const char *payload, bool retain, bool full_topic)
    {
        // Method to publish a message
        if (__connected_on_boot)
        {
            // If 'full_topic' is 'false', we have to create a full topic,
            // otherwise, we can assume the given topic is a full topic
            char send_topic[128];
            if (!full_topic)
            {
                cpy_topic(send_topic, topic);
            }
            else
            {
                strcpy(send_topic, topic);
            }
            // Method to send a message
            __logger->debug("DIOT_MQTT_Client", "Sending message to \"", topic, "\"");
            return __mqtt_client.publish(send_topic, payload, retain);
        }

        return false;
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::update_status()
    {
        // Method to update the status of the client

        // Create the data for the uptime
        char uptime[12];
        sprintf(uptime, "%.3f", (float)millis() / 1000);

        // Create the buffer for the connects
        char connects[6];

        // Update the status
        send_message(TOPIC_STATUS_AVAILABILITY, PAYLOAD_STATUS_AVAILABILITY_ONLINE, true, false);
        send_message(TOPIC_STATUS_UPTIME, uptime, false, false);
        send_message(TOPIC_STATUS_CONNECTS, itoa(__connects, connects, 10), false, false);

        // Set the time of the last uptime status
        __last_status_update = millis();
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::update_about()
    {
        // Update the 'about' section
        if (__application)
        {
            send_message(TOPIC_ABOUT_DIOT_VERSION, __application->get_version(), true, false);
            send_message(TOPIC_ABOUT_DIOT_BRANCH, __application->get_branch(), true, false);
            send_message(TOPIC_ABOUT_APP_NAME, __application->get_app_name(), true, false);
            send_message(TOPIC_ABOUT_APP_VERSION, __application->get_app_version(), true, false);
            send_message(TOPIC_ABOUT_APP_BRANCH, __application->get_app_branch(), true, false);
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::subscription_callback(const char *topic, byte *payload, unsigned int length)
    {
        // The callback for the subscriptions

        // Extract the exact command from the topic
        char cmd[128];
        uint16_t command_index = 0;
        for (uint16_t i = strlen(__topic_commands) - 1; i < strlen(topic) + 1; i++)
        {
            cmd[command_index] = topic[i];
            command_index++;
        }

        // Put the payload in a char array
        char cmd_payload[512];
        memcpy(cmd_payload, payload, length);
        cmd_payload[length] = '\0';

        __logger->debug("DIOT_MQTT_Client", "Callback for topic: \"", topic, "\", command: \"", cmd, "\" payload: ", cmd_payload);

        // Check if this is a 'default' command
        if (strcmp(cmd, "diot_restart") == 0)
        {
            // Command to restart the device
            __logger->debug("DIOT_MQTT_Client", "Command given to restart the device");
            DIOT::restart();
        }
        else
        {
            // Unknown command
            __logger->debug("DIOT_MQTT_Client", "Unknown command");

            // Check if a command callback is given, and if it is, execute it
            if (callback_cmds)
            {
                callback_cmds(cmd, cmd_payload);
            }
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_MQTT_Client::cpy_topic(char *destination, const char *topic)
    {
        // Method that returns the full topic name for a specific topic. For
        // example; if the MQTT prefix for this application is:
        //
        // CB-KR23/Frontyard/Doorbell
        //
        // And the given 'topic' is
        //
        // status/status
        //
        // The result will be:
        //
        // CB-KR23/Frontyard/Doorbell/status/status

        // Create a local buffer for the new topic
        char new_topic[128];
        strcpy(new_topic, __topic);

        // First, check if the topic has a trailing '/'. If it doesn't have that
        // we can add it
        if (new_topic[strlen(new_topic) - 1] != '/')
        {
            strcat(new_topic, "/");
        }

        // Then, add the given topic
        strcat(new_topic, topic);

        // Place the created topic into the buffer
        strcpy(destination, new_topic);
    }
}
//------------------------------------------------------------------------------