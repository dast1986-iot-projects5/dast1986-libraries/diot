#ifndef __diot_mqtt_h__
#define __diot_mqtt_h__
//------------------------------------------------------------------------------
// Includes
#include <WiFi.h>
#include <PubSubClient.h>
#include "logger.h"
#include "configblock.h"
#include "diot.h"
//------------------------------------------------------------------------------
// Static configuration
#define MQTT_KEEPALIVE_TIMEOUT 5
#define DEFAULT_STATUS_UPDATE_TIMEOUT 60
#define TOPIC_STATUS_AVAILABILITY "status/status"
#define PAYLOAD_STATUS_AVAILABILITY_ONLINE "online"
#define PAYLOAD_STATUS_AVAILABILITY_OFFLINE "offline"
#define TOPIC_STATUS_UPTIME "status/uptime"
#define TOPIC_STATUS_CONNECTS "status/mqtt_connects"
#define TOPIC_ABOUT_DIOT_VERSION "about/diot_version"
#define TOPIC_ABOUT_DIOT_BRANCH "about/diot_branch"
#define TOPIC_ABOUT_APP_NAME "about/app_name"
#define TOPIC_ABOUT_APP_VERSION "about/app_version"
#define TOPIC_ABOUT_APP_BRANCH "about/app_branch"
//------------------------------------------------------------------------------
namespace diot
{
    // Class for a MQTT client
    class DIOT_MQTT_Client
    {
    private:
        // Pointer to a logger
        DIOT_Logger *__logger;

        // Pointer to the DIOT applications ConfigList
        DIOT_ConfigBlock *__cfglist;
        uint16_t *__cfglist_size;

        // Pointer to the application
        DIOT *__application;

        // Status
        bool __connected_on_boot;

        // Client settings
        bool *__enabled;
        uint16_t *__port;
        char *__server;
        char *__clientid;
        char *__username;
        char *__password;
        char *__topic;

        // Topics
        char __topic_commands[128];

        // Client objects
        WiFiClient __wifi_client; // Needed for the mqtt object
        PubSubClient __mqtt_client;

        // Timeouts and counters
        unsigned long __last_status_update;
        uint16_t __connects;

    public:
        // Constructors and destructors
        DIOT_MQTT_Client(DIOT_ConfigBlock *cfglist, uint16_t *cfglist_size);

        // Methods for the ConfigBlocks
        int16_t find_empty_configblock();

        // Methods to setup the client
        void add_configuration();

        // Client methods
        void begin(DIOT *application);
        bool connect();
        void loop();
        bool send_message(const char *topic, const char *payload, bool retain, bool full_topic);
        void update_status();
        void update_about();
        void subscription_callback(const char *topic, byte *payload, unsigned int length);

        // Topic methods
        void cpy_topic(char *destination, const char *topic);

        // Timeouts
        uint16_t status_update_timeout;

        // Callback method for commands
        std::function<void(const char *command, const char *payload)> callback_cmds;
    };
}
//------------------------------------------------------------------------------
#endif