#ifndef _diot_gpio_digital_input_h_
#define _diot_gpio_digital_input_h_
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include "gpio.h"
//------------------------------------------------------------------------------
namespace diot
{
    // Class to control a digital input GPIO, like buttons
    class GPIO_Digital_Input : public GPIO
    {
    private:
        uint8_t __value_active;
        uint8_t __value_inactive;
        bool __pullup;

    public:
        // Constructors and destructors
        GPIO_Digital_Input(uint8_t gpio = 0, bool pullup = true);

        // Overloaded operators
        operator bool();

        // Methods to setup the input GPIO
        void set_pullup(bool pullup = true);
        void init_gpio();
        void set_values(uint8_t active, uint8_t inactive);

        // Methods to read the input GPIO
        bool active() const;
    };
}
//------------------------------------------------------------------------------
#endif