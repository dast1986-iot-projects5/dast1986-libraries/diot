#ifndef _diot_gpio_h_
#define _diot_gpio_h_
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
//------------------------------------------------------------------------------
namespace diot
{
    // Base class for GPIO related derived classes
    class GPIO
    {
    protected:
        uint8_t _gpio;

    public:
        // Constructors and destructors
        GPIO(uint8_t gpio = 0);

        // Methods to setup the GPIO
        void set_gpio(uint8_t gpio);

        // Virtual methods for derived classes
        virtual void init_gpio() = 0;
    };
}
//------------------------------------------------------------------------------
#endif