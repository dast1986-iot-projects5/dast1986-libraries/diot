// Includes
#include "gpio_digital_input.h"
//------------------------------------------------------------------------------
namespace diot
{
    GPIO_Digital_Input::GPIO_Digital_Input(uint8_t gpio /* = 0 */, bool pullup /* = true */)
        : GPIO(gpio)
    {
        // Default constructor sets default values
        __pullup = pullup;
        set_values(LOW, HIGH);
    }
    //--------------------------------------------------------------------------
    GPIO_Digital_Input::operator bool()
    {
        // Overload for the 'bool' type. This can be used to sense the state of
        // the GPIO with a simple 'if' statement
        return active();
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Input::set_pullup(bool pullup /* = true */)
    {
        // Set the pullup
        __pullup = pullup;
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Input::init_gpio()
    {
        // Initialize the GPIO
        pinMode(_gpio, __pullup ? INPUT_PULLUP : INPUT);
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Input::set_values(uint8_t active, uint8_t inactive)
    {
        // Set the values for 'active' and 'inactive'
        __value_active = active;
        __value_inactive = inactive;
    }
    //--------------------------------------------------------------------------
    bool GPIO_Digital_Input::active() const
    {
        // Returns if the GPIO is active
        return digitalRead(_gpio) == __value_active;
    }
}
//------------------------------------------------------------------------------