#ifndef __diot_setting_h__
#define __diot_setting_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include <Preferences.h>
#include <functional>
#include "logger.h"
//------------------------------------------------------------------------------
// Static configuration
#define TYPE_TEXT 1
#define TYPE_TEXT_PASSWORD 2
#define TYPE_BOOL 3
#define TYPE_UINT16 4
#define TYPE_INFO_STRING 128
#define TYPE_INFO_UINT8 129
#define TYPE_INFO_UINT16 130
#define TYPE_INFO_FUNCTION_STRING 160
//------------------------------------------------------------------------------
namespace diot
{
    class DIOT_Setting
    {
    private:
        // Objects for preferences
        Preferences _preferences;

        // Internal objects
        DIOT_Logger *__logger;

    public:
        DIOT_Setting();

        // Setters
        void set_partition(const char *setting_partition);

        // Methods to configure settings
        void define_text(const char *setting_id, const char *setting_title, const char *default_value, bool password = false);
        void define_bool(const char *setting_id, const char *setting_title, bool default_value);
        void define_uint16(const char *setting_id, const char *setting_title, uint16_t default_value);

        // Methods to configure information
        void define_info_text(const char *setting_title, char *string_pointer, const char *setting_id = "", const char *default_value = "");
        void define_info_uint8(const char *setting_title, uint8_t *uint8_pointer, const char *setting_id = "", uint8_t default_value = 0);
        void define_info_uint16(const char *setting_title, uint16_t *uint16_pointer, const char *setting_id = "", uint16_t default_value = 0);

        // Methods to configure information functions
        void define_info_function_string(const char *setting_title, std::function<void(char *)> function);

        // Methods to save and retrieve data from Flash memory
        void retrieve();
        void save();

        // Methods to get pointers to the values
        char *get_pointer_string();
        bool *get_pointer_bool();
        uint16_t *get_pointer_uint16();

        // Properties for the setting
        bool configured;
        bool visible;
        char title[32];
        uint8_t type;
        char id[16];
        char partition[16];

        // Value for the settings
        char string_value[128];
        bool bool_value;
        uint16_t uint16_value;

        // Default values for settings
        char string_default[128];
        bool bool_default;
        uint16_t uint16_default;
        uint8_t uint8_default;

        // Pointers for 'information' settings
        char *pointer_string;
        uint8_t *pointer_uint8;
        uint16_t *pointer_uint16;

        // Pointers for 'information function' settings
        std::function<void(char *)> function_string;
    };
}
//------------------------------------------------------------------------------
#endif