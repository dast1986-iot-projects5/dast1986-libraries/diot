#ifndef __diot_h__
#define __diot_h__
//------------------------------------------------------------------------------
// Static configuration
#define DIOT_NAME "DIOT"
#define DIOT_VERSION "1.0.6"
#define DIOT_BRANCH "Production"
#define CFGLIST_SIZE 8
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include "logger.h"
#include "configblock.h"
#include "gpio_digital_output.h"
//------------------------------------------------------------------------------
namespace diot
{
    // Class that represents a IOT application
    class DIOT
    {
    protected:
        // Variables for DIOT information
        char _diot_version[12];
        char _diot_branch[16];

        // Variables for application information
        char _app_name[32];
        uint8_t _app_version[3];
        char _app_branch[16];
        char _app_version_complete[12];

        // Objects that the application can and should use
        DIOT_Logger *_logger;

        // List with configurationblocks
        DIOT_ConfigBlock _cfglist[CFGLIST_SIZE];
        uint16_t _cfglist_size;

        // Objects for the onboard LED
        GPIO_Digital_Output _onboard_led;

    public:
        // Constructors and destructors
        DIOT(const char *name, uint8_t version_major, uint8_t version_minor, uint8_t version_revision, const char *branch, unsigned short configlist_size = 0, uint8_t serial_severity_logging = DEBUG);

        // Methods for the configblocks
        int16_t find_empty_configblock();

        // Methods that inherited classes should always implement
        virtual void setup() = 0;
        virtual void loop() = 0;

        // Methods to retrieve the application information
        const char *get_version() const;
        const char *get_branch() const;
        const char *get_app_name() const;
        const char *get_app_version() const;
        const char *get_app_branch() const;

        // Methods for the 'runtime information' block
        void get_uptime(char *destination_buffer) const;
        void get_free_heap_space(char *destination_buffer) const;
        void get_minimum_free_heap_space(char *destination_buffer) const;
        void get_sketch_size(char *destination_buffer) const;

        // Methods to control the board
        static void restart();
    };
}
//------------------------------------------------------------------------------
#endif
