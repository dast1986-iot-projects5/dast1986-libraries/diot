#ifndef __diot_configblock_h__
#define __diot_configblock_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include "setting.h"
#include "logger.h"
#include "types.h"
//------------------------------------------------------------------------------
// Static configuration
#define SETTING_SIZE 8
//------------------------------------------------------------------------------
namespace diot
{
    class DIOT_ConfigBlock
    {
    private:
        // Internal objects
        DIOT_Logger *__logger;

    public:
        // Constructors and destructors
        DIOT_ConfigBlock();

        // Configurationblock management
        void define(const char *block_title, const char *block_id, uint8_t block_type);
        int16_t find_empty_setting() const;

        // Methods to retrieve and save settings
        void retrieve_settings();
        void save_settings();

        // Properties
        uint8_t type;
        bool configured;
        char title[32];
        char id[16];
        bool needed_in_setup;

        // Settings
        DIOT_Setting settings[SETTING_SIZE];
        uint16_t setting_size;
    };
}
//------------------------------------------------------------------------------
#endif