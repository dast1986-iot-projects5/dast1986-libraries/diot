// Includes
#include "gpio_digital_output.h"
//------------------------------------------------------------------------------
namespace diot
{
    GPIO_Digital_Output::GPIO_Digital_Output(uint8_t gpio /* = 0 */)
        : GPIO(gpio)
    {
        // Default constructor sets default values
        set_values(HIGH, LOW);
        __on = false;
        __auto_toggle_interval = 0;
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::init_gpio()
    {
        // Configure the GPIO for output
        pinMode(_gpio, OUTPUT);
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::set_values(uint8_t on, uint8_t off)
    {
        // Set the values for 'on' and 'off'
        __value_on = on;
        __value_off = off;
        }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::on(unsigned int period /* = 0 */)
    {
        // Method to turn on the GPIO
        digitalWrite(_gpio, __value_on);
        __on = true;

        // If a period is set, we set the time at which the GPIO should be
        // turned off
        if (period > 0)
        {
            __turn_off_time = millis() + period;
        }
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::off(unsigned int period /* = 0 */)
    {
        // Method to turn off the GPIO
        digitalWrite(_gpio, __value_off);
        __on = false;

        // If a period is set, we set the time at which the GPIO should be
        // turned on
        if (period > 0)
        {
            __turn_on_time = millis() + period;
        }
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::toggle()
    {
        // Method to toggle the GPIO
        if (__on)
        {
            off();
        }
        else
        {
            on();
        }
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::start_auto_toggle(uint16_t interval)
    {
        // Method to set the GPIO to toggling automatically at a specific
        // interval. To make this work, the user has to use the loop() method of
        // the GPIO object on every iteration of the ESP loop() method
        __auto_toggle_interval = interval;
        __last_toggle = millis();
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::stop_auto_toggle(bool turn_off /* = true */)
    {
        // Stops __auto_toggle_interval auto toggle of the GPIO
        __auto_toggle_interval = 0;

        if (turn_off)
        {
            off();
        }
    }
    //--------------------------------------------------------------------------
    void GPIO_Digital_Output::loop()
    {
        // Method that does the things that need to be done for the GPIO in the
        // loop iteration

        // If auto toggle is one, we check if we need to toggle it
        if (__auto_toggle_interval > 0)
        {
            if (millis() - __last_toggle >= __auto_toggle_interval)
            {
                toggle();
                __last_toggle = millis();
            }
        }

        // If there is a 'turn off' timer set, and we are at, or past that
        // time, we have to turn off the GPIO
        if (__turn_off_time > 0 && millis() >= __turn_off_time)
        {
            off();
        }

        // If there is a 'turn on' timer set, and we are at, or past that
        // time, we have to turn on the GPIO
        if (__turn_on_time > 0 && millis() >= __turn_on_time)
        {
            on();
        }
    }
}
//------------------------------------------------------------------------------