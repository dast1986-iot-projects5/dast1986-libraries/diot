// Includes
#include "setting.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_Setting::DIOT_Setting()
    {
        // The default constructor sets default values
        configured = false;
        visible = true;

        // Set the local logger
        __logger = DIOT_Logger::get_instance();
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::set_partition(const char *setting_partition)
    {
        // Method to set the partition for the setting
        strcpy(partition, setting_partition);

        __logger->debug("Setting", "Setted the partition for this setting to \"", setting_partition, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_text(const char *setting_id, const char *setting_title, const char *default_value, bool password /* = false */)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = (password) ? TYPE_TEXT_PASSWORD : TYPE_TEXT;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        strcpy(string_default, default_value);
        strcpy(string_value, default_value);

        __logger->debug("Setting", "Defined setting \"", setting_title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_bool(const char *setting_id, const char *setting_title, bool default_value)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_BOOL;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        bool_value = default_value;
        bool_default = default_value;

        __logger->debug("Setting", "Defined setting \"", setting_title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_uint16(const char *setting_id, const char *setting_title, uint16_t default_value)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_UINT16;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        uint16_value = default_value;
        uint16_default = default_value;

        __logger->debug("Setting", "Defined setting \"", setting_title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_info_text(const char *setting_title, char *string_pointer, const char *setting_id /* = "" */, const char *default_value /* = "" */)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_INFO_STRING;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        pointer_string = string_pointer;
        strcpy(string_default, default_value);

        __logger->debug("Setting", "Defined info \"", setting_title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_info_uint8(const char *setting_title, uint8_t *uint8_pointer, const char *setting_id /* = "" */, uint8_t default_value /* = 0 */)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_INFO_UINT8;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        pointer_uint8 = uint8_pointer;
        uint8_default = default_value;

        __logger->debug("Setting", "Defined info \"", title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_info_function_string(const char *setting_title, std::function<void(char *)> function)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_INFO_FUNCTION_STRING;
        strcpy(title, setting_title);

        // Set the value
        function_string = function;
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::define_info_uint16(const char *setting_title, uint16_t *uint16_pointer, const char *setting_id /* = "" */, uint16_t default_value /* = 0 */)
    {
        // Method to configure the setting
        configured = true;

        // Set the parameters
        type = TYPE_INFO_UINT16;
        strcpy(title, setting_title);
        strcpy(id, setting_id);

        // Set the default value
        pointer_uint16 = uint16_pointer;
        uint16_default = default_value;

        __logger->debug("Setting", "Defined info \"", title, "\"");
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::retrieve()
    {
        // Method to retrieve the setting from flash memory

        if (strcmp(id, "") != 0)
        {
            // Create a preferences object and set it to RO mode
            _preferences.begin(partition, true);

            // Retrieve the setting
            switch (type)
            {
            case TYPE_TEXT:
            case TYPE_TEXT_PASSWORD:
                // Get the value of the setting
                _preferences.getString(id, string_value, 128);
                break;
            case TYPE_BOOL:
                // Get the value of the bool
                bool_value = _preferences.getBool(id, bool_default);
                break;
            case TYPE_UINT16:
                // Get the value of the unsigned short
                uint16_value = _preferences.getUShort(id, uint16_default);
                break;
            case TYPE_INFO_STRING:
                // Get the value of the string
                _preferences.getString(id, pointer_string, 128);
                break;
            case TYPE_INFO_UINT8:
                // Get the value of the unsigned 8 bit integer
                *pointer_uint8 = _preferences.getUChar(id, uint8_default);
                break;
            case TYPE_INFO_UINT16:
                // Get the value of the unsigned short
                *pointer_uint16 = _preferences.getUShort(id, uint16_default);
                break;
            }

            // Done with the preferences object
            _preferences.end();
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_Setting::save()
    {
        // Method to save the setting to flash memory

        if (strcmp(id, "") != 0)
        {
            __logger->debug("Setting", "Saving setting \"", title, "\"");

            // Create a preferences object and set it to Rw mode
            _preferences.begin(partition, false);

            // Save the setting
            switch (type)
            {
            case TYPE_TEXT:
            case TYPE_TEXT_PASSWORD:
                // Save the value of the saved string
                _preferences.putString(id, String(string_value));
                break;
            case TYPE_BOOL:
                // Save the value of the saved boolean
                _preferences.putBool(id, bool_value);
                break;
            case TYPE_UINT16:
                // Save the value of the unsigned short
                _preferences.putUShort(id, uint16_value);
                break;
            case TYPE_INFO_STRING:
                // Save the value for the string
                _preferences.putString(id, String(pointer_string));
                break;
            case TYPE_INFO_UINT8:
                // Save the value for the unsigned char
                _preferences.putUChar(id, *pointer_uint8);
                break;
            case TYPE_INFO_UINT16:
                // Save the value of the unsigned short
                _preferences.putUShort(id, *pointer_uint16);
                break;
            }

            // Done with the preferences object
            _preferences.end();
        }
    }
    //--------------------------------------------------------------------------
    char *DIOT_Setting::get_pointer_string()
    {
        // Method to get a pointer to the 'string' value. Can be used to change
        // strings without needing the methods for this object
        return &string_value[0];
    }
    //--------------------------------------------------------------------------
    bool *DIOT_Setting::get_pointer_bool()
    {
        // Method to get a pointer to the 'bool' value. Can be used to change
        // boolean without needing the methods for this object
        return &bool_value;
    }
    //--------------------------------------------------------------------------
    uint16_t *DIOT_Setting::get_pointer_uint16()
    {
        // Method to get a pointer to the 'uint16' value. Can be used to change
        // uint16 without needing the methods for this object
        return &uint16_value;
    }
}