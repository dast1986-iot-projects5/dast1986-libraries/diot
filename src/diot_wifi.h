#ifndef __diot_wifi_h__
#define __diot_wifi_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include <WiFi.h>
#include "webserver.h"
#include "diot.h"
#include "mqtt.h"
//------------------------------------------------------------------------------
// Static configuration
#define DEFAULT_SSID_PREFIX "DarylIOT - "
#define WIFI_CONNECT_LOOP_TIMEOUT 1000
#define MAX_RETRIES 8
//------------------------------------------------------------------------------
namespace diot
{
    // Class that represents a IOT application with WiFi
    class DIOT_WiFi : public DIOT
    {
    private:
        // Configblock locations
        int16_t __configblock_location;

        // Objects to keep WiFi information
        char *__ssid;
        char *__password;
        bool *__startapmode;

        // State objects
        bool __state_ap_mode;

    protected:
        // Methods that derived classes should use in their own versions of
        // these methods
        void _setup();
        bool _loop();

        // Communication objects
        DIOT_WebServer _webserver;
        DIOT_MQTT_Client _mqtt_client;

    public:
        // Constructors and destructors
        DIOT_WiFi(const char *name, uint8_t version_major, uint8_t version_minor, uint8_t version_revision, const char *branch, unsigned short configlist_size = 0, uint8_t serial_severity_logging = DEBUG);

        // Methods that inherited classes should always implement
        virtual void setup() = 0;
        virtual void loop() = 0;

        // Method to connect
        bool connect();
    };
}
//------------------------------------------------------------------------------
#endif