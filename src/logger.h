#ifndef __diot_logger_h__
#define __diot_logger_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include <stdio.h>
//------------------------------------------------------------------------------
// Loglevel defines
#define EMERGENCY 0
#define ALERT 1
#define CRITICAL 2
#define ERROR 3
#define WARNING 4
#define NOTICE 5
#define INFO 6
#define DEBUG 7
#define NO_LOGGING 255
//------------------------------------------------------------------------------
// Static configuration
#define DEFAULT_LOGLEVEL NOTICE
//------------------------------------------------------------------------------
namespace diot
{
    // Class for a logger
    class DIOT_Logger
    {
    private:
        // Variable to hold the original instance
        static DIOT_Logger *__instance;

        // Severity levels
        uint8_t __serial_severity;

        // Flag if the Serial has started so we won't do that again
        bool __serial_started;

        // Private constructor. We make this private so it won't be possible to
        // create a instance of it. If you need an instance of this class, you
        // have to use the 'getInstance()' method like this:
        //
        // DIOT_Logger* logger = DIOT_Logger::get_instance()
        //
        // logger->debug("Module", "Created logger");
        //
        DIOT_Logger();

    public:
        // Static member to create a instance of this class
        static DIOT_Logger *get_instance();

        // Remove the copy constructor
        DIOT_Logger(const DIOT_Logger &logger) = delete;

        // Method to define the logging channels
        void set_serial_severity(uint8_t serial_severity);

        // Methods for logging
        void log(uint8_t severity, const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void emergency(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void alert(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void critical(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void error(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void warning(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void notice(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void info(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
        void debug(const char *module, const char *line1, const char *line2 = NULL, const char *line3 = NULL, const char *line4 = NULL, const char *line5 = NULL, const char *line6 = NULL);
    };
}
//------------------------------------------------------------------------------
#endif