#ifndef _diot_gpio_digital_output_h_
#define _diot_gpio_digital_output_h_
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include "gpio.h"
//------------------------------------------------------------------------------
namespace diot
{
    // Class to control digital output GPIOs, like LEDs or relays
    class GPIO_Digital_Output : public GPIO
    {
    private:
        uint8_t __value_on;
        uint8_t __value_off;
        bool __on;
        uint16_t __auto_toggle_interval;
        unsigned long __last_toggle;
        unsigned long __turn_on_time;
        unsigned long __turn_off_time;

    public:
        // Constructors and destructors
        GPIO_Digital_Output(uint8_t gpio = 0);

        // Methods to setup the LED
        void init_gpio();
        void set_values(uint8_t on, uint8_t off);

        // Methods to control the LED
        void on(unsigned int period = 0);
        void off(unsigned int period = 0);
        void toggle();
        void start_auto_toggle(uint16_t interval);
        void stop_auto_toggle(bool turn_off = true);

        // Looping methods
        void loop();
    };
}
//------------------------------------------------------------------------------
#endif