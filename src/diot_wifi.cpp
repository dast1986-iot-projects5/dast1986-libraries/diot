// Includes
#include "diot_wifi.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_WiFi::DIOT_WiFi(const char *name, uint8_t version_major, uint8_t version_minor, uint8_t version_revision, const char *branch, unsigned short configlist_size /* = 0 */, uint8_t serial_severity_logging /* = DEBUG */)
        : DIOT(name, version_major, version_minor, version_revision, branch, configlist_size, serial_severity_logging), _webserver(_app_name, &_cfglist[0], &_cfglist_size), _mqtt_client(&_cfglist[0], &_cfglist_size)
    {
        _logger->debug("DIOT_WiFi", "Constructor started");

        // Add a configurationblock for WiFi

        __configblock_location = find_empty_configblock();
        if (__configblock_location >= 0)
        {
            // Add the block
            char buffer[6];
            _logger->debug("DIOT_WiFi", "Adding a ConfigBlock at position: ", itoa(__configblock_location, buffer, 10));

            // Configure the block
            _cfglist[__configblock_location].define("WiFi configuration", "wifi", TYPE_CONFIG);
            _cfglist[__configblock_location].needed_in_setup = true;
            _cfglist[__configblock_location].settings[0].define_text("ssid", "SSID", "");
            _cfglist[__configblock_location].settings[1].define_text("password", "Password", "", true);
            _cfglist[__configblock_location].settings[2].define_bool("startapmode", "Start in AP mode", false);
            _cfglist[__configblock_location].settings[2].visible = false;

            // Set the internal variables for the settings
            __ssid = _cfglist[__configblock_location].settings[0].get_pointer_string();
            __password = _cfglist[__configblock_location].settings[1].get_pointer_string();
            __startapmode = _cfglist[__configblock_location].settings[2].get_pointer_bool();
        }
        else
        {
            // Send a debug logging so the programmer knows to fix the issue
            _logger->error("DIOT_WiFi", "No free ConfigBlocks available! Increase the CFGLIST_SIZE size!");
        }

        // Set 'AP mode' to false. We can set it to true later
        __state_ap_mode = false;

        _logger->debug("DIOT_WiFi", "Constructor done");
    }
    //--------------------------------------------------------------------------
    void DIOT_WiFi::_setup()
    {
        _logger->debug("DIOT_WiFi", "Starting setup()");

        // Retrieve the settings from memory
        _cfglist[__configblock_location].retrieve_settings();

        // Add the configurationblock for the communication clients
        _webserver.add_configuration();
        _mqtt_client.add_configuration();

        if (*__startapmode || strcmp(__ssid, "") == 0)
        {
            _logger->info("DIOT_WiFi", "WiFi not configured or \"startapmode\" flag set. Starting in AP mode");
            __state_ap_mode = true;

            // Set the LED to blinking
            _onboard_led.start_auto_toggle(500);

            // If '__startapmode' flag is set, we reset this.
            if (__startapmode)
            {
                *__startapmode = false;
                _cfglist[__configblock_location].save_settings();
            }

            // Create a SSID name
            char ssid_name[64] = DEFAULT_SSID_PREFIX;
            strcat(ssid_name, _app_name);
            char logbuffer[88] = "Starting AP with SSID: ";
            _logger->info("DIOT_WiFi", strcat(logbuffer, ssid_name));

            // Start the Access Point
            WiFi.softAP(ssid_name);

            // Wait two seconds before doing the rest. If we don't do this, it
            // is possible the complete system crashes
            delay(2000);

            // Setup the webserver
            _webserver.setup_mode(true); // Set it to 'setup_mode'
            _webserver.begin();
        }
        else
        {
            _logger->info("DIOT_WiFi", "WiFi is configured and flag for AP mode is not set, starting normally");

            // Connect to WiFi
            if (!connect())
            {
                // We couldn't connect to WiFi. Since this happend during the
                // boot-process, we restart the device in 'AP mode' so the user
                // can reconfigure WiFi if needed
                _logger->error("DIOT_WiFi", "Couldn't connect to WiFi! Restarting in AP mode");

                if (!*__startapmode)
                {
                    *__startapmode = true;
                    _cfglist[__configblock_location].save_settings();
                }

                // Restart the device
                DIOT::restart();
            }
            else
            {
                _logger->info("DIOT_WiFi", "Connected!");

                // Set 'start_ap_mode' to false so the next time we startup, it
                // won't go to AP mode
                if (*__startapmode)
                {
                    *__startapmode = false;
                    _cfglist[__configblock_location].save_settings();
                }

                // Setup the MQTT client
                _mqtt_client.begin(this);

                // Setup the webserver
                _webserver.setup_mode(false); // Set it to 'setup_mode'
                _webserver.begin();
            }
        }

        // Done!
        _logger->debug("DIOT_WiFi", "Done with setup()");
    }
    //--------------------------------------------------------------------------
    bool DIOT_WiFi::_loop()
    {
        _logger->debug("DIOT_WiFi", "Starting loop()");

        if (!__state_ap_mode)
        {
            // Loop MQTT
            _mqtt_client.loop();

            // Check if we still have WiFi
            if (WiFi.status() != WL_CONNECTED)
            {
                _logger->notice("DIOT_WiFi", "Connection to WiFi lost");
                if (!connect())
                {
                    _logger->error("DIOT_WiFi", "Couldn't reconnect to WiFi. Making sure the device will start in AP mode on next try");
                    if (!*__startapmode)
                    {
                        *__startapmode = true;
                        _cfglist[__configblock_location].save_settings();
                    }
                    return true;
                }
                else
                {
                    _logger->info("DIOT_WiFi", "Reconnected!");
                    if (*__startapmode)
                    {
                        *__startapmode = false;
                        _cfglist[__configblock_location].save_settings();
                    }
                }
            }
        }
        else
        {
            // If we are in 'AP mode', we loop the LED
            _onboard_led.loop();
        }

        // Make sure the communication method get looped
        _webserver.loop();

        // Done
        _logger->debug("DIOT_WiFi", "Done with loop()");

        return !__state_ap_mode;
    }
    //--------------------------------------------------------------------------
    bool DIOT_WiFi::connect()
    {
        // Method to connect to WiFi

        _logger->info("DIOT_WiFi", "Connecting to WiFi \"", __ssid, "\"");

        // Set the starttime
        unsigned long starttime = millis();
        uint16_t try_count = 0;

        // Start connceting
        while (WiFi.status() != WL_CONNECTED)
        {
            // Get the counter; we keep track of the amount of times we have
            // tried to connect. If we exceed a specified amount, we abort the
            // connection and return 'false'. The calling code can decide what
            // to do about this.
            try_count++;

            if (try_count < MAX_RETRIES)
            {
                // Not exceed the max retires. Let's connect!
                char buffer[6];
                _logger->info("DIOT_WiFi", "Try: ", itoa(try_count, buffer, 10));

                // Connect!
                WiFi.begin(__ssid, __password);
                delay(WIFI_CONNECT_LOOP_TIMEOUT);

                // Check if we get a connection
                unsigned short count = 0;
                if (WiFi.status() != WL_CONNECTED && count < 3)
                {
                    delay(WIFI_CONNECT_LOOP_TIMEOUT);
                    count++;
                }
            }
            else
            {
                // We tried to many times, stop trying
                _logger->info("DIOT_WiFi", "To many tries!");
                return false;
            }
        }

        return true;
    }
}
//------------------------------------------------------------------------------