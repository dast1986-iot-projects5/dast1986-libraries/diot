// Includes
#include "webserver_page.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_WebServerPage::DIOT_WebServerPage()
    {
        // Constructor sets default values
        enabled = true;
        visible = true;
    }
}
//------------------------------------------------------------------------------