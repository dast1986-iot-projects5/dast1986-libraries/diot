// Includes
#include "configblock.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT_ConfigBlock::DIOT_ConfigBlock()
    {
        // The default constructor sets default values
        configured = false;
        needed_in_setup = false;

        // Set the default size for the list
        setting_size = SETTING_SIZE;

        // Set the local logger
        __logger = DIOT_Logger::get_instance();
    }
    //--------------------------------------------------------------------------
    void DIOT_ConfigBlock::define(const char *block_title, const char *block_id, uint8_t block_type)
    {
        // Method to set the title and iD for a ConfigBlock

        // Set 'configured' to true, so the caller knows this block is set
        configured = true;

        if (__logger)
        {
            // Send a log message to the debug-channel to let know that this
            // block is configured
            char buffer[255];
            strcpy(buffer, "Configuring configblock with title '");
            strcat(buffer, block_title);
            strcat(buffer, "' and id '");
            strcat(buffer, block_id);
            strcat(buffer, "'");

            __logger->debug("ConfigBlock", buffer);
        }

        // Set the title and id
        strcpy(title, block_title);
        strcpy(id, block_id);

        // Set the type
        type = block_type;

        // Set the partition (= the id for this block) for all the settings in
        // the block
        for (uint16_t i = 0; i < setting_size; i++)
        {
            settings[i].set_partition(id);
        }
    }
    //--------------------------------------------------------------------------
    int16_t DIOT_ConfigBlock::find_empty_setting() const
    {
        // Find the first empty setting
        for (uint8_t i = 0; i < setting_size; i++)
        {
            if (!settings[i].configured)
            {
                // Found the number; return it
                return i;
            }
        }

        // Return a negative number
        return -1;
    }
    //--------------------------------------------------------------------------
    void DIOT_ConfigBlock::retrieve_settings()
    {
        // Method to walk through the settings and retrieve their value from the
        // memory
        for (uint8_t i = 0; i < setting_size; i++)
        {
            if (settings[i].configured)
            {
                if (__logger)
                {
                    char buffer[255] = "Retrieving setting: ";
                    __logger->debug("ConfigBlock", strcat(buffer, settings[i].id));
                }
                settings[i].retrieve();
            }
        }
    }
    //--------------------------------------------------------------------------
    void DIOT_ConfigBlock::save_settings()
    {
        // Method to walk through the settings and save their value to the
        // memory
        for (uint8_t i = 0; i < setting_size; i++)
        {
            if (settings[i].configured)
            {
                if (__logger)
                {
                    char buffer[255] = "Saving setting: ";
                    __logger->debug("ConfigBlock", strcat(buffer, settings[i].id));
                }
                settings[i].save();
            }
        }
    }
}
//------------------------------------------------------------------------------