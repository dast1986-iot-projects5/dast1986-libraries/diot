#ifndef __diot_webserver_h__
#define __diot_webserver_h__
//------------------------------------------------------------------------------
// Includes
#include <Arduino.h>
#include <WebServer.h>
#include <HTTP_Method.h>
#include "diot.h"
#include "webserver_page.h"
#include "configblock.h"
#include "logger.h"
#include "types.h"
#include "gpio_digital_output.h"
//------------------------------------------------------------------------------
// Static configuration
#define PAGE_COUNT 3
#define RELOAD_TIME 3000
// Buffer sizes
#define BUFFER_SIZE_FULL_PAGE 8192
#define BUFFER_SIZE_CFGBLOCKS 6144
#define BUFFER_SIZE_SETTING 128
#define BUFFER_SIZE_BLOCK 1024
#define BUFFER_SIZE_MENU 512
//------------------------------------------------------------------------------
namespace diot
{
    class DIOT_WebServer
    {
    private:
        // Objects for the mode
        bool __setup_mode;

        // Object for the webserver
        WebServer __web_server;

        // List with pages
        uint8_t __homepage;
        DIOT_WebServerPage __pages[PAGE_COUNT];

        // Pointer to a logger
        DIOT_Logger *__logger;

        // Pointer to the DIOT applications ConfigList
        DIOT_ConfigBlock *__cfglist;
        uint16_t *__cfglist_size;

        // Webserver settings
        uint16_t *__port;
        char *__username;
        char *__password;

        // Application properties
        char *__app_name;

        // Local buffers; can be used for templates. Is defined in this space so
        // it doesn't have to be used in the stack
        char __temp_html_page_buffer[BUFFER_SIZE_FULL_PAGE];
        char __temp_html_cfg_blocks_buffer[BUFFER_SIZE_CFGBLOCKS];

    public:
        // Constructors and destructors
        DIOT_WebServer(char *application_name, DIOT_ConfigBlock *cfglist, uint16_t *cfglist_size);

        // Methods for the ConfigBlocks
        int16_t find_empty_configblock();

        // Methods to setup the webserver
        void setup_mode(bool setup_mode);
        void add_configuration();
        void set_routing();

        // Webserver methods
        void begin();
        void loop();
        void webserver_serve_page(uint8_t page_id, boolean save = false);
        void webserver_save_config(uint8_t page_id);
    };
}
//------------------------------------------------------------------------------
#endif