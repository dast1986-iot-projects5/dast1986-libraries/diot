// Includes
#include "diot.h"
//------------------------------------------------------------------------------
namespace diot
{
    DIOT::DIOT(const char *name, uint8_t version_major, uint8_t version_minor, uint8_t version_revision, const char *branch, unsigned short configlist_size /* = 0 */, uint8_t serial_severity_logging /* = DEBUG */)
    {
        // Configure the builtin LED and turn it on
        _onboard_led.set_gpio(LED_BUILTIN);
        _onboard_led.on();

        // Set the logger and configure it
        _logger = DIOT_Logger::get_instance();
        _logger->set_serial_severity(serial_severity_logging);

        _logger->debug("DIOT_Base", "Constructor started");

        // Set the DIOT version and branch (needed for ConfigBlock)
        strcpy(_diot_version, DIOT_VERSION);
        strcpy(_diot_branch, DIOT_BRANCH);

        // Set the application name, version and branch
        strcpy(_app_name, name);
        strcpy(_app_branch, branch);
        _app_version[0] = version_major;
        _app_version[1] = version_minor;
        _app_version[2] = version_revision;

        // Create a 'complete' string for the app version
        char buffer[4];
        strcpy(_app_version_complete, itoa(_app_version[0], buffer, 10));
        strcat(_app_version_complete, ".");
        strcat(_app_version_complete, itoa(_app_version[1], buffer, 10));
        strcat(_app_version_complete, ".");
        strcat(_app_version_complete, itoa(_app_version[2], buffer, 10));

        // Send a log message
        _logger->info("DIOT_Base", DIOT_NAME, " (", DIOT_VERSION, " - ", DIOT_BRANCH, ") is starting");
        _logger->info("DIOT_Base", _app_name, " (", _app_version_complete, " - ", _app_branch, ") is starting");

        // Set variables for the lists
        _cfglist_size = CFGLIST_SIZE;

        // Create the first configblock; a block with information about the
        // application
        _logger->debug("DIOT_Base", "Adding configuration block with version information");
        _cfglist[0].define("About", "about", TYPE_ABOUT);
        _cfglist[0].needed_in_setup = false;
        _cfglist[0].settings[0].define_info_text("Daryl IOT version", &_diot_version[0]);
        _cfglist[0].settings[1].define_info_text("Daryl IOT branch", &_diot_branch[0]);
        _cfglist[0].settings[2].define_info_text("Application name", &_app_name[0]);
        _cfglist[0].settings[3].define_info_text("Application version", &_app_version_complete[0]);
        _cfglist[0].settings[4].define_info_text("Application branch", &_app_branch[0]);

        _logger->debug("DIOT_Base", "Adding configuration block with runtime information");
        _cfglist[1].define("Runtime information", "runtime", TYPE_ABOUT);
        _cfglist[1].needed_in_setup = false;
        _cfglist[1].settings[0].define_info_function_string("System uptime", std::bind(&DIOT::get_uptime, this, std::placeholders::_1));
        _cfglist[1].settings[1].define_info_function_string("Available heap", std::bind(&DIOT::get_free_heap_space, this, std::placeholders::_1));
        _cfglist[1].settings[2].define_info_function_string("Minimum available heap", std::bind(&DIOT::get_minimum_free_heap_space, this, std::placeholders::_1));
        _cfglist[1].settings[3].define_info_function_string("Sketch size", std::bind(&DIOT::get_sketch_size, this, std::placeholders::_1));

        _logger->debug("DIOT_Base", "Constructor done");
    }
    //--------------------------------------------------------------------------
    int16_t DIOT::find_empty_configblock()
    {
        // Find the first non configured configblock
        for (uint8_t i = 0; i < _cfglist_size; i++)
        {
            if (!_cfglist[i].configured)
            {
                // Found the number; return it
                return i;
            }
        }

        // Return a negative number
        return -1;
    }
    //--------------------------------------------------------------------------
    const char *DIOT::get_version() const
    {
        // Return the DIOT version
        return _diot_version;
    }
    //--------------------------------------------------------------------------
    const char *DIOT::get_branch() const
    {
        // Return the DIOT branch
        return _diot_branch;
    }
    //--------------------------------------------------------------------------
    const char *DIOT::get_app_name() const
    {
        // Return the application name
        return _app_name;
    }
    //--------------------------------------------------------------------------
    const char *DIOT::get_app_version() const
    {
        // Return the application version
        return _app_version_complete;
    }
    //--------------------------------------------------------------------------
    const char *DIOT::get_app_branch() const
    {
        // Return the application branch
        return _app_branch;
    }
    //--------------------------------------------------------------------------
    void DIOT::get_uptime(char *destination_buffer) const
    {
        // Method that copies the uptime of the device in human readable format
        // to the specified buffer

        // Get the counters for the uptime
        unsigned long seconds = millis() / 1000;
        uint8_t days = seconds / 3600 / 24;
        seconds -= days * 3600 * 24;
        uint8_t hours = seconds / 3600;
        seconds -= hours * 3600;
        uint8_t minutes = seconds / 60;
        seconds -= minutes * 60;

        // Set the buffer to a string with only seconds
        sprintf(destination_buffer, "%ds", seconds);
        if (minutes > 0)
        {
            // Add minutes
            sprintf(destination_buffer, "%dm and %ds", minutes, seconds);
        }
        if (hours > 0)
        {
            // Add hours
            sprintf(destination_buffer, "%dh, %dm and %ds", hours, minutes, seconds);
        }
        if (days > 0)
        {
            // Add days
            sprintf(destination_buffer, "%dd, %dh, %dm and %ds", days, hours, minutes, seconds);
        }
    }
    //--------------------------------------------------------------------------
    void DIOT::get_free_heap_space(char *destination_buffer) const
    {
        // Method that returns the available heap space
        sprintf(destination_buffer, "%d bytes", ESP.getFreeHeap());
    }
    //--------------------------------------------------------------------------
    void DIOT::get_minimum_free_heap_space(char *destination_buffer) const
    {
        // Method that returns the minimum of available heap size that was
        // available during executing of the application
        sprintf(destination_buffer, "%d bytes", ESP.getMinFreeHeap());
    }
    //--------------------------------------------------------------------------
    void DIOT::get_sketch_size(char *destination_buffer) const
    {
        // Method that returns the sketch size
        sprintf(destination_buffer, "%d bytes", ESP.getSketchSize());
    }
    //--------------------------------------------------------------------------
    void DIOT::restart()
    {
        // Method to restart the device
        ESP.restart();
    }
}
//------------------------------------------------------------------------------